## Read in the motifs from HOMER DB
library(tidyverse)
devtools::load_all()

## Construct paths to HOMER db motif sets (all)
base <- marge::get_homer_base()
mset <- c('vertebrates', 'insects', 'plants', 'worms', 'yeast')
paths <- paste0(base, '/data/knownTFs/', mset, '/all.motifs')

## Function to read in each motif and add 'mset' (motif set) var
.read_org_motifs <- function(path, mset) {
    read_denovo_results(
        path = path,
        homer_dir = FALSE
    ) %>%
        data.frame(mset = mset, .) %>%
        as_tibble
}

HOMER_motifs <- map2(paths, mset, .read_org_motifs) %>%
    bind_rows()

## Save data
devtools::use_data(HOMER_motifs, overwrite = TRUE, compress = 'xz')


